import React from "react";
import { StyleSheet, TextInput} from 'react-native';
import {VARS} from "../styles/styles";

export class AppInput extends React.PureComponent {
	constructor (props) {
		super(props);
		this.state = {hasFocus: false};
	}

	render () {
		return (<TextInput
				style={this.getStyles(this.state.hasFocus)}
				onFocus={this.setFocus.bind(this, true)}
				onBlur={this.setFocus.bind(this, false)}
				underlineColorAndroid='rgba(0,0,0,0)'
		/>);
	}
	getStyles(hasFocus){
		return hasFocus ? {...AppInputStyles.focusedTextInput, ...this.props.style} : {...AppInputStyles.textInput, ...this.props.style};
	}
	setFocus (hasFocus) {
		this.setState({hasFocus});
	}
};

const AppInputStyles = StyleSheet.create({
	textInput: {...VARS.input},
	focusedTextInput: {
		...VARS.input,
		borderColor: VARS.color.indigo
	}
});