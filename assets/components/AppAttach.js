import React from "react";
import {
	StyleSheet,
	Text, View
} from 'react-native';
import {VARS} from "../styles/styles";
import Image from 'react-native-remote-svg';

const APP_ATTACH_ICONS = {
	doc: require('../styles/images/doc.svg'),
	pdf: require('../styles/images/pdf.svg'),
	jpg: require('../styles/images/jpg.svg')
};

export class AppAttach extends React.PureComponent {
	constructor(props){
		super(props);
		this.state = {
			icon: null
		}
	}
	componentDidMount(){
		let {type} = this.props;
		this.setState({
			icon: APP_ATTACH_ICONS[type]
		})
	}
	render () {
		let {type, title} = this.props;
		let {icon} = this.state;
		return (
			<View style={AppAttachStyles.attach}>
				<View style={AppAttachStyles.icon}>
					{!!icon && <Image source={icon} />}
				</View>
				<Text numberOfLines={1} style={{...VARS.text.attach, flexShrink:1}}>
					{title}
				</Text>
				<Text style={{...VARS.text.attach}}>
					.{type}
				</Text>
			</View>
		);
	}
};

const AppAttachStyles = StyleSheet.create({
	attach: {flexDirection: 'row', justifyContent: 'flex-start'},
	icon: {marginRight: 6, marginTop: 2}
});