import React from "react";
import {StyleSheet, View} from 'react-native';
import {VARS} from "../styles/styles";

export class AppCard extends React.PureComponent {
	render () {
		return (<View style={{...this.props.style, ...AppCardStyles.card}}>
			{this.props.children}
		</View>);
	}
};

const AppCardStyles = StyleSheet.create({
	card: {
		backgroundColor: VARS.color.white,
		borderRadius: 3,
		marginBottom: 12,
		paddingVertical: 18,
		paddingHorizontal: 16
	}
});