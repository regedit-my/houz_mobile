import React from "react";
import {
	StyleSheet,
	View
} from 'react-native';
import {
	Header,
	Icon
} from 'react-native-elements';
import Image from 'react-native-remote-svg';
import {
	OPTIONS,
	VARS
} from "../styles/styles";

export class AppHeader extends React.PureComponent {
	constructor(props) {
		super(props);
		this.handleClick = props.navigate.bind(this, 'Home')
	}

	render() {
		return (
				<Header
						leftComponent={<View style={{marginTop: 12}}><Icon name="more-horiz" size={40} color={VARS.color.white}/></View>}
						centerComponent={<Image source={require('../styles/images/houz_w.svg')} style={{...AppHeaderStyleSheet.logo}}/>}
						rightComponent={<Icon onPress={this.handleClick} name="home" size={30} color={VARS.color.white}/>}
						outerContainerStyles={{...OPTIONS.innerPageHeader.headerStyle, justifyContent: 'flex-end', margin: 0, paddingBottom: 8}}/>)
	}
};

const AppHeaderStyleSheet = StyleSheet.create({
	header: {
		backgroundColor: VARS.color.indigo
	},
	logo: {
		width: 80,
		height: 38
	}
});