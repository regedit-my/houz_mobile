import React from "react";
import {
	KeyboardAvoidingView,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from 'react-native';
import Image from 'react-native-remote-svg';
import {AppInput} from '../components/AppInput'
import {VARS} from "../styles/styles";

export class Login extends React.Component {
	static navigationOptions = {
		headerTintColor: VARS.color.primary,
		headerStyle: {
			backgroundColor: '#F2F7FF',
			borderBottomWidth: 0,
		}
	};
	render() {
		return (
			<KeyboardAvoidingView behavior="padding" style={LoginStyleSheet.container}>
				<Image
					source={require('../styles/images/houz.svg')}
					style={LoginStyleSheet.logo}
				/>
				<View style={LoginStyleSheet.form}>
					<View style={LoginStyleSheet.inputBox}>
						<View style={LoginStyleSheet.inputBoxLabel}>
							<Text style={LoginStyleSheet.inputBoxLabelLeft}>Адрес</Text>
						</View>
						<AppInput style={LoginStyleSheet.input}/>
					</View>
					<View style={LoginStyleSheet.inputBox}>
						<View style={LoginStyleSheet.inputBoxLabel}>
							<Text style={LoginStyleSheet.inputBoxLabelLeft}>Номер квартиры</Text>
						</View>
						<AppInput style={LoginStyleSheet.input}/>
					</View>
					<View style={LoginStyleSheet.inputBox}>
						<View style={LoginStyleSheet.inputBoxLabel}>
							<Text style={LoginStyleSheet.inputBoxLabelLeft}>Пароль</Text>
							<Text style={LoginStyleSheet.inputBoxLabelRight}>Забыли пароль?</Text>
						</View>
						<AppInput style={LoginStyleSheet.input}/>
					</View>
					<TouchableOpacity style={LoginStyleSheet.submit}>
						<Text style={LoginStyleSheet.submitText}>Войти</Text>
					</TouchableOpacity>
				</View>
			</KeyboardAvoidingView>
		);
	}
}

const LoginStyleSheet = StyleSheet.create({
	container: { backgroundColor: '#F2F7FF', flex: 1, justifyContent: 'center', alignItems: 'center'},
	logo: { width: 130, height: 60},
	form: { width: 255, marginTop: 10},
	submit: {...VARS.button.primary, marginTop: 26},
	input: { marginTop: 6},
	inputBox: { marginTop: 16},
	inputBoxLabel: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'baseline'},
	inputBoxLabelLeft: { fontSize: 14, color: '#6C757D'},
	inputBoxLabelRight: { fontSize: 12, color: '#98A6AD'},
	submitText: {color: 'white'}
});