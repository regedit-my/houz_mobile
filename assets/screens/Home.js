import React from "react";
import { View, Button } from 'react-native';

export class Home extends React.Component {
	render() {
		return (
			<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
				<Button title="Стартовый экран" onPress={() => this.props.navigation.navigate('Loading')} />
				<Button title="Авторизация" onPress={() => this.props.navigation.navigate('Login')} />
				<Button title="Карточки" onPress={() => this.props.navigation.navigate('AppCards')} />
			</View>
		);
	}
}