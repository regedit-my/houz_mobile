import React from "react";
import {
	StyleSheet,
	Text,
	ScrollView,
	View
} from 'react-native';
import {
	Icon
} from 'react-native-elements';
import {VARS} from '../styles/styles';
import {AppHeader} from "../components/AppHeader";
import {AppCard} from "../components/AppCard";
import {AppAttach} from "../components/AppAttach";

export class AppCards extends React.Component {
	static navigationOptions = ({navigation}) => {
		return {
			header: <AppHeader {...navigation} />
		}
	};

	render() {
		return (
			<View style={CardsStyleSheet.container}>
				<ScrollView>
					<View style={CardsStyleSheet.view}>
						<AppCard>
							<View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 6}}>
								<View style={{flexDirection: 'row', flex: 1}}>
									<Text style={{...VARS.text.info, marginRight: 20}}># 38</Text>
									<View style={{...VARS.badge.success}}>
										<Text style={{...VARS.text.badge}}>Открыт</Text>
									</View>
								</View>
								<View>
									<Icon name="more-horiz" color={VARS.color.icon}/>
								</View>
							</View>
							<Text style={{...VARS.text.title, marginBottom: 6}}>
								Выберите цвет новых замечательных входных дверей
							</Text>
							<Text ellipsizeMode={'tail'} numberOfLines={3} style={{...VARS.text.paragraph, color: VARS.color.gray}}>
								Двери от местного надежного производителя утепленные по самой современной финской технологии.
							</Text>
							<View style={{marginTop: 8}}>
								<View style={{flexDirection: 'row', flex: 1, marginBottom: 6}}>
									<Text style={{...VARS.text.select, marginRight: 6}}>1.</Text>
									<Text style={{...VARS.text.select, flexShrink:1}}>Черный — 2</Text>
								</View>
								<View style={{flexDirection: 'row', flex: 1, marginBottom: 6}}>
									<Text style={{...VARS.text.selectActive, marginRight: 6}}>2.</Text>
									<Text style={{...VARS.text.selectActive, flexShrink:1}}>Серый — 3</Text>
								</View>
								<View style={{flexDirection: 'row', flex: 1, marginBottom: 6}}>
									<Text style={{...VARS.text.select, marginRight: 6}}>3.</Text>
									<Text style={{...VARS.text.select, flexShrink:1}}>Коричневый (очень близко к
										  замечательному оттенку мечтательной
										  блохи) — 4</Text>
								</View>
							</View>
							<View style={{
								marginHorizontal: -CardsStyleSheet.view.paddingHorizontal,
								paddingHorizontal: CardsStyleSheet.view.paddingHorizontal,
								...CardsStyleSheet.cardPool}}>
								<View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 10}}>
									<Text style={{...VARS.text.poll}}>Проголосовало 11 из 20</Text>
									<Text style={{...VARS.text.poll}}>55%</Text>
								</View>
								<View style={{flexDirection: 'row', flex: 1, backgroundColor: '#E9ECEF', height: 5}}>
									<View style={{height: 5, width: 100, backgroundColor: VARS.color.indigo}} />
								</View>
							</View>
						</AppCard>
						<AppCard>
							<View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 6}}>
								<View style={{flexDirection: 'row', flex: 1}}>
									<Text style={{...VARS.text.info, marginRight: 20}}># 38</Text>
									<Text style={{...VARS.text.info}}>24.11.2018</Text>
								</View>
								<View />
							</View>
							<Text style={{...VARS.text.title, marginBottom: 6}}>
								Установить урны
							</Text>
							<Text ellipsizeMode={'tail'} numberOfLines={3} style={{...VARS.text.paragraph}}>
								В связи с тем, что урны для мусора постоянное переполнены, нужно установить еще 2 урны, потом необходимо доработать
							</Text>
							<View style={{marginTop: 8}}>
								<AppAttach type={'pdf'} title={'Акт выполненных работ'} />
								<AppAttach type={'jpg'} title={'Описание забора'} />
								<AppAttach type={'doc'} title={'Подробное описание недостатков по всем участкам работы'} />
							</View>
						</AppCard>
						<AppCard>
							<Text style={{...VARS.text.ahead, marginBottom: 16}}>
								ЧУП «Управляющая компания»
								ТСЖ дома №1 по ул. Академическая
							</Text>
							<Text style={{...VARS.text.title, marginBottom: 16}}>
								Мирошниченко
								Александр Леонидович
							</Text>
							<View style={{marginBottom: 10}}>
								<Text style={{...VARS.text.subtitle, marginBottom: 5}}>Телефон</Text>
								<Text style={{color: VARS.color.blue}}>+375 29 643-34-34</Text>
							</View>
							<View style={{marginBottom: 10}}>
								<Text style={{...VARS.text.subtitle, marginBottom: 5}}>Часы приема</Text>
								<Text style={{...VARS.text.paragraph}}>Понедельник с 9:00 до 16:00, вторник-среда 9:00-10:00, четверг-пятница с 15:00 до 17:00.</Text>
							</View>
						</AppCard>
						<AppCard>
							<Text style={{...VARS.text.title, marginBottom: 8}}>
								Службы
							</Text>
							<View style={{marginBottom: 10}}>
								<Text style={{...VARS.text.subtitle, marginBottom: 5}}>Аварийная служба</Text>
								<Text style={{color: VARS.color.blue}}>+375 29 643-34-34</Text>
							</View>
							<View style={{marginBottom: 10}}>
								<Text style={{...VARS.text.subtitle, marginBottom: 5}}>Сантехник</Text>
								<Text style={{color: VARS.color.blue}}>+375 29 643-34-34</Text>
							</View>
							<View style={{marginBottom: 10}}>
								<Text style={{...VARS.text.subtitle, marginBottom: 5}}>Газовики</Text>
								<Text style={{color: VARS.color.blue}}>+375 29 643-34-34</Text>
							</View>
							<View style={{marginBottom: 10}}>
								<Text style={{...VARS.text.subtitle, marginBottom: 5}}>Электрик</Text>
								<Text style={{color: VARS.color.blue}}>+375 29 643-34-34</Text>
							</View>
						</AppCard>
					</View>
				</ScrollView>
			</View>
		);
	}
}

const CardsStyleSheet = StyleSheet.create({
	container: {
		backgroundColor: "#EDEFF7",
		flex: 1,
	},
	cardPool: {
		borderTopWidth: 1,
		borderTopColor: '#E3EAEF',
		paddingTop: 12,
		marginTop: 12
	},
	view: {
		paddingHorizontal: 16,
		paddingTop: 24
	}
});