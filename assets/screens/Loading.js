import React from "react";
import { View, StyleSheet } from 'react-native';
import { VARS } from '../styles/styles';
import Image from 'react-native-remote-svg';

export class Loading extends React.Component {
	static navigationOptions = {
		headerTintColor: 'white',
		headerStyle: {
			backgroundColor: VARS.color.primary,
			borderBottomWidth: 0,
		}
	};
	render() {
		return (
			<View style={LoadingStyleSheet.container}>
				<Image
					source={require('../styles/images/houz_w.svg')}
					style={LoadingStyleSheet.logo}
				/>
			</View>
		);
	}
}

const LoadingStyleSheet = StyleSheet.create({
	container: { backgroundColor: VARS.color.primary, flex: 1, alignItems: 'center', justifyContent: 'center' },
	logo: { width: 130, height: 60}
});