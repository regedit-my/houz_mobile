const colors = {
	blue: '#6670F1',
	indigo: '#727cf5',
	purple: '#6b5eae',
	pink: '#ff679b',
	red: '#fa5c7c',
	orange: '#fd7e14',
	yellow: '#ffbc00',
	green: '#0acf97',
	teal: '#02a8b5',
	cyan: '#39afd1',
	white: '#fff',
	gray: '#98a6ad',
	grayDark: '#343a40',
	primary: '#727cf5',
	secondary: '#6c757d',
	success: '#0acf97',
	info: '#39afd1',
	warning: '#ffbc00',
	danger: '#fa5c7c',
	light: '#e3eaef',
	dark: '#313a46',
	icon: '#3F4657'
};

const commonStyles = {
	input: {
		paddingHorizontal: 12,
		borderWidth: 1,
		borderRadius: 3,
		backgroundColor: 'white',
		borderColor: '#C6CFFC',
		height: 44,
		fontSize: 16
	},
	button: {
		paddingVertical: 12,
		fontWeight: '500',
		fontSize: 17,
		alignItems: 'center'
	},
	badge: {
		paddingHorizontal: 4,
		height: 20,
		borderRadius: 3
	}
};

const commonOptionStyles = {
	innerPageHeader: {
		headerTintColor: 'white',
		headerStyle: {
			backgroundColor: colors.primary,
			borderBottomWidth: 0,
		}
	}
};

export const OPTIONS = {
	innerPageHeader: {
		...commonOptionStyles.innerPageHeader
	}
};

export const VARS = {
	color: {
		...colors
	},
	button: {
		primary: {
			...commonStyles.button,
			shadowColor: 'rgb(114,124,245)',
			shadowOpacity: 0.35,
			shadowRadius: 3,
			elevation: 6,
			shadowOffset: {
				width: 2,
				height: 6
			},
			backgroundColor: colors.primary,
			borderRadius: 3,
			color: 'white'
		}
	},
	text: {
		ahead: {
			fontFamily: 'OpenSansLight',
			color: 'rgba(0,0,0,0.7)',
			fontSize: 14,
			lineHeight: 21
		},
		title: {
			fontFamily: 'RubikMedium',
			color: 'rgb(0,0,0)',
			fontSize: 17,
			lineHeight: 24
		},
		subtitle: {
			fontFamily: 'OpenSansSemiBold',
			color: 'rgba(0,0,0,0.7)',
			fontSize: 14,
			lineHeight: 20
		},
		paragraph: {
			fontFamily: 'OpenSansRegular',
			fontSize: 13,
			lineHeight: 20
		},
		attach: {
			fontFamily: 'OpenSansRegular',
			fontSize: 13,
			lineHeight: 20,
			color: 'rgba(0,0,0,0.5)'
		},
		info: {
			fontSize: 13,
			lineHeight: 20,
			color: 'rgba(0,0,0,0.5)'
		},
		badge: {
			color: colors.white,
			fontSize: 11,
			lineHeight: 18,
		},
		poll: {
			fontFamily: 'OpenSansSemiBold',
			color: colors.secondary,
			fontSize: 14,
			lineHeight: 21,
		},
		select: {
			fontFamily: 'OpenSansRegular',
			fontSize: 14,
			lineHeight: 21
		},
		selectActive: {
			fontFamily: 'OpenSansBold',
			color: colors.indigo,
			fontSize: 14,
			lineHeight: 21,
		},
	},
	badge: {
		success: {
			backgroundColor: colors.success,
			...commonStyles.badge
		},
		secondary: {
			backgroundColor: colors.secondary,
			...commonStyles.badge
		}
	},
	input: {
		...commonStyles.input
	}
};