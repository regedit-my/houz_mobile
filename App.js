import React from 'react';
import { StyleSheet } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { Font } from 'expo';

//screens
import { Home } from './assets/screens/Home';
import { Login } from './assets/screens/Login';
import { Loading } from './assets/screens/Loading';
import { AppCards } from './assets/screens/AppCards';

const RootStack = createStackNavigator({
	Login,
	Loading,
	AppCards,
	Home: {
		screen: Home,
		navigationOptions: {
			header: null
		}
	}
}, {
	initialRouteName: 'Home',
});

export default class App extends React.Component {
	componentDidMount() {
		Font.loadAsync({
			'OpenSansLight': require('./assets/styles/fonts/OpenSansLight.ttf'),
			'OpenSansRegular': require('./assets/styles/fonts/OpenSansRegular.ttf'),
			'OpenSansSemiBold': require('./assets/styles/fonts/OpenSansSemiBold.ttf'),
			'OpenSansBold': require('./assets/styles/fonts/OpenSansBold.ttf'),
			'RubikMedium': require('./assets/styles/fonts/RubikMedium.ttf'),
		});
	}
	render() {
		return <RootStack />;
	}
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
